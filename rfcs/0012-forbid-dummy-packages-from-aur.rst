======================================================
Adoption of stance against “dummy” packages in the AUR
======================================================

- Date proposed: 2022-03-23
- RFC MR: https://gitlab.archlinux.org/archlinux/rfcs/-/merge_requests/12

Summary
-------

Reject AUR packages that fulfill package dependencies without providing the
files/binaries of the package in question.

Motivation
----------

“Dummy” packages (packages with no contents that exist only to satisfy
dependencies) and their acceptance is a topic that's never reached concensus.

An example dummy PKGBUILD:

.. code-block::

   pkgname=ttf-font-nerd-dummy
   pkgver=0.1.0
   pkgrel=1
   epoch=
   pkgdesc="Dummy package to provide ttf-font-nerd"
   arch=("any")
   # url=
   license=('MIT OR Apache 2.0')
   groups=()
   depends=()
   makedepends=()
   checkdepends=()
   optdepends=()
   provides=("ttf-font-nerd")
   conflicts=()
   replaces=()
   backup=()
   options=()
   install=
   changelog=
   noextract=()
   validpgpkeys=()

This package serves no functional purpose; It exists to satisfy a dependency
without having the files installed on the system. This allows the user to
install other packages without having to include a larger, unwanted package on
the system.

While the existence of these packages are relatively harmless, the lack of
clarity on their approval makes AUR deletion requests more difficult. The
package maintainer can push back, claiming no documentation expressly
forbidding such packages. Thus far, removal of such packages have been one of
personal preference of the requester. Ratifying our disapproval of these
packages will make justification of deletion much easier in these instances.

Other OSes, such as Debian, do utilize dummy packages, but they are usually for
facilitating transitions during OS upgrades. The dummy packages in the context
of the AUR exist solely to fool the package manager into believing a package
requirement is fulfilled.

Specification
-------------

The following line would be added to https://wiki.archlinux.org/title/AUR_submission_guidelines#Rules_of_submission

.. code-block::
    * AUR packages must not circumvent the dependency requirements of other packages (“dummy packages”).

Drawbacks
---------

Push-back from a small subset of AUR users, though seemingly few dummy packages
exist in the AUR.

Unresolved Questions
--------------------

Are “shim” packages in scope of this RFC? For instance:

.. code-block::
    pkgname=pulseaudio-dummy
    pkgver=0.2
    pkgrel=1
    pkgdesc='Dummy PulseAudio package to utilize apulse'
    arch=('i686' 'x86_64')
    license=('custom:MIT')
    depends=('apulse')
    provides=('pulseaudio' 'pulseaudio-alsa' 'libpulse' 'libpulse.so=0-64' 'libpulse-simple.so=0-64')
    conflicts=(${provides[@]})

    package() {
        mkdir -p ${pkgdir}/usr/lib
        for lib in libpulse-mainloop-glib.so libpulse-simple.so libpulse.so
        do
            ln -s apulse/${lib} "${pkgdir}/usr/lib/${lib}"
            ln -s apulse/${lib}.0 "${pkgdir}/usr/lib/${lib}.0"
        done
    }

Would this be deleted or instead renamed to e.g. pulseaudio-apulse-shim?

Alternatives Considered
-----------------------

None
